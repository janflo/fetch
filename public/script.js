const RSS_URL = `https://corsproxy.io/?https://push.superfeedr.com/?hub.mode=retrieve&hub.callback=https%3A%2F%2Fpush.superfeedr.com%2Fdev%2Fnull&count=200&format=atom&authorization=amFuZmxvOmM1NGQ4YzAyNDY2ZGMyYTRjODgyN2UzZDAyZjkxYjRh&`;

fetch(RSS_URL)
  .then(response => response.text())
  .then(str => new window.DOMParser().parseFromString(str, "text/xml"))
  .then(data => {
    console.log(data);
    const items = data.querySelectorAll("entry");
    let html = ``;
    items.forEach(el => {
      html += `
        <article>
          <small>	${el.querySelector("updated").innerHTML}
				
					</small>
          <h2>
            <a href="${el.querySelector("link").attributes[2].nodeValue}" target="_blank" rel="noopener">
              ${el.querySelector("title").innerHTML}
            </a>
					
          </h2>
					<p>
					${el.querySelector("summary").textContent.replace(/<\/?[^>]+>/gi,"")}
					</p>
					<div class="stopka">
					 <small >	
					${el.querySelector("source").childNodes[1].innerHTML}
					</small>
					</div>
        </article>
      `;
    });
    document.body.insertAdjacentHTML("beforeend", html);
  });
